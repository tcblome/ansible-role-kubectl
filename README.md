Ansible role: kubectl
=========

![pipeline](https://gitlab.com/tcblome/ansible-role-kubectl/badges/master/build.svg)

This role installs [kubernetes kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/).


Requirements
------------
none

Installation
------------

Using `ansible-galaxy`:

```shell
$ ansible-galaxy install https://gitlab.com/tcblome/ansible-role-kubectl.git
```

Using `requirements.yml`:
```yaml
 src: git+https://gitlab.com/tcblome/ansible-role-kubectl.git
 name: tcblome.kubectl
```
Using `git`:
```shell
$ git clone https://gitlab.com/tcblome/ansible-role-kubectl.git tcblome.kubectl
```

Role Variables
--------------
```yaml

# defaults file for ansible-role-kubectl
#
# kubectl_version	*	# version of kubectl to install

```

Dependencies
------------

This role can be used independently.

Example Playbook
----------------

Sample :
```
    - hosts: servers
      roles:
        - { role: tcblome.kubectl,
	  kubectl_version: "*"
	 }
```

License
------------------
[LICENSE](LICENSE)

Author Information
------------------

This role is published for private use by @tcblome
